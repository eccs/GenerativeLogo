
class Piece{
  // Percentage of corner displacement from original position
  static final float offset = 0.2;

  // Original width and height units
  static final float wo = 1;
  static final float ho = 1.5;

  static final float textS = 1.2;

  // Margin between pieces
  static final float marginh = 0.3;
  static final float marginv = 0.3;

  // Stroke Weight
  static final float strokeW = 0.1;


  // Colors
  final color colorWhite = color(255);
  final color colorBlack = color(0);



  // The String to show inside the piece
  String text;

  // Show or not the text
  boolean showText; 

  // Number of horizontal units
  int nhunits;

  // Original width of this piece
  float w;

  // Original top and left corners
  PVector pos;

  // Vertices 
  PVector [] v;

  // Joint types
  int jointRight,jointLeft;

  // Color state: 
  // 1 - White on Black
  // 2 - Black on White
  int colorState;

  Piece(String t){
    text = t;

    // The number of units correspond to the text length
    nhunits = text.length();

    // The original width is the number of units times the length
    w = nhunits * wo;

    pos = new PVector(0,0);


    v = new PVector[6];
    for(int i=0; i<v.length; i++){
      v[i] = new PVector(0,0);
    }

    // Vertex initialization in a rect
    v[0].set(0,0);
    v[1].set(w,0);
    v[2].set(w,ho/2);
    v[3].set(w,ho);
    v[4].set(0,ho);
    v[5].set(0,ho/2);


    // Default color state
    colorState = 1;

    showText = true;


  }

  int setRandomColorState(){
    colorState = (random(1)<0.5)? 1 : 2;
    return colorState;
  }

  // Set position
  void setPosition(float x, float y){
    pos = new PVector(x,y);

  }


  void setPositionAtRightOf(Piece p){
    PVector tr = p.getTopRight();
    pos = new PVector(tr.x + marginh, tr.y);
  }

  void setPositionAtLeftOf(Piece p){
    PVector tl = p.getTopLeft();
    pos = new PVector(tl.x - marginh - getTopRight().x+pos.x, tl.y);

  }

  void setPositionBelowLeftOf(Piece p){
    PVector bl = p.getBottomLeft();
    pos = new PVector(bl.x, bl.y + marginv);
  }

  void setPositionBelowRightOf(Piece p){
    PVector br = p.getBottomRight();
    pos = new PVector(br.x - getTopRight().x+pos.x, br.y + marginv);
  }

  void setPositionBelow(Piece p, boolean left){
   if(left){
    setPositionBelowLeftOf(p);
   }
   else{
    setPositionBelowRightOf(p);
   }
  }


  // Set joints and change corners accordingly
  void setJointRight(int jointType){
    jointRight = jointType;
    updateJointRight();
  }

  void setJointLeft(int jointType){
    jointLeft = jointType;
    updateJointLeft();
  }

  private void updateJointRight(){
    switch(jointRight){
      case Joint.GT:
        v[1].x = w - offset;
	v[3].x = w - offset;
      break;
      case Joint.LT:
        v[1].x = w + offset;
	v[3].x = w + offset;
      break;
      case Joint.FS:
        v[1].x = w + offset;
	v[3].x = w - offset;
      break;
      case Joint.BS:
        v[1].x = w - offset;
	v[3].x = w + offset;
      break;
      case Joint.V:
        v[1].x = w;
	v[3].x = w;
      break;
    }
    v[2].x = w ;
  }

  private void updateJointLeft(){
    switch(jointLeft){
      case Joint.GT:
        v[0].x =  0;
	v[5].x = offset;
	v[4].x =  0;
      break;
      case Joint.LT:
        v[0].x = 0;
	v[5].x = -offset;
	v[4].x = 0;
      break;
      case Joint.FS:
        v[0].x = 0;
	v[5].x = -offset;
	v[4].x = -2*offset;
      break;
      case Joint.BS:
        v[0].x = 0;
	v[5].x = offset;
	v[4].x = offset*2;
      break;
      case Joint.V:
        v[0].x = 0;
	v[5].x = 0;
	v[4].x = 0;
      break;
    }

  }


  // Get corners
  PVector getTopLeft(){
    return PVector.add(v[0],pos);
  }

  PVector getTopRight(){
    return PVector.add(v[1],pos);
  }

  PVector getBottomLeft(){
    return PVector.add(v[4],pos);
  }

  PVector getBottomRight(){
    return PVector.add(v[3],pos);
  }


  PVector getRightmost(){
    if(v[1].x >= v[2].x){
      if(v[1].x>=v[3].x){
        return PVector.add(v[1],pos);
      }
    }
    else{
      if(v[2].x>=v[3].x){
	return PVector.add(v[2],pos);
      }
    }
    return PVector.add(v[3],pos);
  }

  PVector getLeftmost(){
    if(v[0].x <= v[5].x){
      if(v[0].x<=v[4].x){
        return PVector.add(v[0],pos);
      }
    }
    else{
      if(v[5].x<=v[4].x){
	return PVector.add(v[5],pos);
      }
    }
    return PVector.add(v[4],pos);
  }

  // The left corner that is rightmost
  PVector getRighttmostLeft(){
    if(v[0].x >= v[5].x){
      if(v[0].x>=v[4].x){
        return v[0];
      }
    }
    else{
      if(v[5].x>=v[4].x){
	return v[5];
      }
    }
    return v[4];
  }

  // The right corner that is leftmost
  PVector getLeftmostRight(){
    if(v[1].x <= v[2].x){
      if(v[1].x<=v[3].x){
        return v[1];
      }
    }
    else{
      if(v[2].x<=v[3].x){
	return v[2];
      }
    }
    return v[3];
  }

  float getRealWidth(){
	return getRightmost().x - getLeftmost().x;
  }




  // DRAW
  void draw(){
    pushMatrix();
    if(colorState == 1){
      stroke(colorBlack);
      fill(colorWhite);
    }
    else{
      stroke(colorWhite);
      fill(colorBlack);
    }

    strokeWeight(strokeW);
    translate(pos.x,pos.y);
    beginShape();
    for(int i=0; i<v.length; i++){
      vertex(v[i].x,v[i].y);  
    }
    endShape(CLOSE);
    if(showText){
     noStroke();
     textSize(textS);
     float textW = textWidth(text);
     float space = getLeftmostRight().x - getRighttmostLeft().x;
     translate(getRighttmostLeft().x+(space-textW)/2,textS*0.9);
     if(colorState == 1){
      fill(colorBlack); 
     }
     else{
      fill(colorWhite); 
     }
     text(text,0,0);
    }
    popMatrix();
  }


}

class Joint{
	static final int GT = 0; // Greater than
	static final int LT = 1; // Less than
	static final int FS = 2; // Forward slash
	static final int BS = 3; // Backslash
	static final int V = 4; // Vertical

	static final int N = 4; // Number of random joints
}
