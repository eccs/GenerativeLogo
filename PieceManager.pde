class PieceManager{
 Piece [] p;

 boolean rArrangement;
 boolean rLeftAlign;
 boolean rBackgroundColor;
 boolean rColorState;
 boolean rJoints;

 int arrangement; // 1 out of 5
 int N_ARRANGEMENTS = 5;

 boolean leftAlign;

 color backgroundColor;

 int [] joints;

 PieceManager(){
  p = new Piece[4];

  p[0] = new Piece("Es");
  p[1] = new Piece("cena");
  p[2] = new Piece("con");
  p[3] = new Piece("sejo");

  joints = new int[3];


  randomizeAll(false);

  // Defaults
  backgroundColor = p[0].colorWhite;
  arrangement = 0;
  leftAlign = true;

  joints[0] = 1;
  joints[1] = 0;
  joints[2] = 3;

  setJoints();

 }

 void generate(){


  if(rBackgroundColor){
   backgroundColor = (random(1)<0.5)?p[0].colorWhite : p[0].colorBlack;
  }
  
  for(int i =0; i<p.length; i++){
    if(rColorState){
      p[i].setRandomColorState();
    }
  }

  if(rJoints){
    generateJoints();
    setJoints();
  }

  if(rArrangement){
   arrangement = int(random(N_ARRANGEMENTS));
  }


  if(rLeftAlign){
   leftAlign = (random(1)<0.5);
  }

  
  switch(arrangement){
   case 0: // Escenaconsejo
    for(int i =1; i<p.length; i++){
      p[i].setPositionAtRightOf(p[i-1]);
    }
   break;
   case 2: // Escena con sejo
    p[1].setPositionAtRightOf(p[0]);
    if(leftAlign){
      p[2].setPositionBelowLeftOf(p[0]);
    }
    else{
      p[2].setPositionBelowRightOf(p[1]);
    }
    p[3].setPositionBelow(p[2],leftAlign);
   break;
   case 1: // Escena consejo
    p[1].setPositionAtRightOf(p[0]);
    if(leftAlign){
      p[2].setPositionBelowLeftOf(p[0]);
      p[3].setPositionAtRightOf(p[2]);
    }
    else{
      p[3].setPositionBelowRightOf(p[1]);
      p[2].setPositionAtLeftOf(p[3]);
    }
   break;
   case 3: // Es cena consejo
     p[1].setPositionBelow(p[0],leftAlign);
     if(leftAlign){
      p[2].setPositionBelowLeftOf(p[1]);
      p[3].setPositionAtRightOf(p[2]);
     }
     else{
      p[3].setPositionBelowRightOf(p[1]);
      p[2].setPositionAtLeftOf(p[3]);
     }
   break;
   case 4: // Es cena con sejo
    for(int i =1; i<p.length; i++){
      p[i].setPositionBelow(p[i-1],leftAlign);
    }
   break;

  }

  println(String.format("Arrangement: %d Left-align: %b",arrangement,leftAlign));

  println(String.format("W: %f H: %f",getTotalWidth(),getTotalHeight()));


 }

 float getTotalHeight(){
  switch(arrangement){
   case 0:
     return p[0].ho;

   case 1:
     return 2*p[0].ho+p[0].marginv;

   case 2:
   case 3:
     return 3*p[0].ho+2*p[0].marginv;

   case 4:
     return 4*p[0].ho+3*p[0].marginv;

  }
  return p[0].ho;
 }

 float getTotalWidth(){
   return getRightmostCorner().x - getLeftmostCorner().x;
 }

 private PVector getLeftmostCorner(){
   float min = p[0].getRightmost().x;
   int index = 0;

   for(int i=0; i<p.length; i++){
     if(p[i].getLeftmost().x < min){
       index = i;
       min = p[i].getLeftmost().x;
     }
   }
   return p[index].getLeftmost();
 }

 private PVector getRightmostCorner(){
   float max = p[0].getLeftmost().x;
   int index = 0;

   for(int i=0; i<p.length; i++){
     if(p[i].getRightmost().x > max){
       index = i;
       max = p[i].getRightmost().x;
     }
   }
   return p[index].getRightmost();
 }

 float scaleToDrawInSquare(float s){
   float w = getTotalWidth();
   float h = getTotalHeight();
   if( w > h ){
     return s/w;
   }
   else{
     return s/h;
   }
 }

 void drawCenteredInSquare(float side, float percentage){
   float scale = scaleToDrawInSquare(side*percentage);
   float posx = (side - scale*getTotalWidth())/2;
   float posy = (side - scale*getTotalHeight())/2;
   draw(posx,posy,scale);
 }


 void draw(float x, float y, float s){
  background(backgroundColor);

  translate(x,y);
  scale(s);

  // Bounding rect
  /*
  noFill();
  stroke(255,0,0);
  rect(0,0,getTotalWidth(),getTotalHeight());
  */

  // Translate to leftmost corner x
  translate(-getLeftmostCorner().x,0);

  // Draw the pieces
  for(int i =0; i<p.length; i++){
   p[i].draw();
  }
 }

 void generateJoints(){
  resetJoints();
  int newjoint;
  for(int i=0; i<joints.length; i++){
   do{
    newjoint = int(random(Joint.N));
   } while(isInJoints(newjoint));
   joints[i] = newjoint;
   println(joints[i]);
  }
 }
 
 void setJoints(){
    p[0].setJointRight(joints[0]);
    p[1].setJointRight(joints[1]);
    p[2].setJointRight(joints[2]);
    p[1].setJointLeft(joints[0]);
    p[2].setJointLeft(joints[1]);
    p[3].setJointLeft(joints[2]);
 }

 void resetJoints(){
  for(int i=0; i<joints.length;i++){
   joints[i] = -1;
  }
 }

 boolean isInJoints(int k){
  for(int i=0; i<joints.length; i++){
   if(joints[i] == k){
    return true;
   }
  }
  return false;
 }


 void randomizeAll(boolean t){
  rArrangement = t;
  rLeftAlign = t;
  rBackgroundColor = t;
  rColorState = t;
  rJoints = t;
 }

 void randomizeJoints(boolean t){
  rJoints = t;
 }

 void randomizeArrangement(boolean t){
  rArrangement = t;
 }

 void randomizeLeftAlign(boolean t){
  rLeftAlign = t;
 }

 void randomizeBackgroundColor(boolean t){
  rBackgroundColor = t;
 }

 void randomizeColorState(boolean t){
  rColorState = t;
 }

 // Setters
 void setArrangement(int a){
  arrangement = a;
 }

 void setLeftAlign(boolean l){
  leftAlign = l;
 }

 void setBackgroundColor(int b){
  backgroundColor = (b==1)?p[0].colorWhite : p[0].colorBlack;
 }




}
