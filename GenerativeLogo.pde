import processing.svg.*;

boolean doRecord = false;
boolean doSave = false;
String pathOutputSVG,pathOutputPNG;

boolean doRandomBackground;

PieceManager pm;

PFont font;

void setup(){
 doRandomBackground = false;

 size(600,600);

 // TO DO
 // PNG and SVG outputs
 int argOutput = argIndex("--svg");
 if(argOutput>=0){
 	doRecord = true;
	pathOutputSVG = argNext(argOutput);
 }

 argOutput = argIndex("--png");
 if(argOutput>=0){
 	doSave = true;
	pathOutputPNG = argNext(argOutput);
 }

 if(argExists("--random-background")){
  doRandomBackground = true;
 }

 font = createFont("SpaceMono-Regular.ttf",42);
 textFont(font);



 pm = new PieceManager();
 pm.randomizeArrangement(true);
 pm.randomizeLeftAlign(true);
 pm.randomizeJoints(true);
 pm.randomizeBackgroundColor(doRandomBackground);
 pm.randomizeColorState(true);
 pm.generate();

 
}

void draw(){

 if(doRecord){
  beginRecord(SVG,pathOutputSVG);
 }

 pushMatrix();

 pm.drawCenteredInSquare(width,0.8);

 popMatrix();

 if(doSave){
  save(pathOutputPNG);
 }


 if(doRecord){
  endRecord();
 }


  if(doSave || doRecord){
   exit();
  }
}

void mousePressed(){
 pm.generate();
}


