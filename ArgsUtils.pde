// Return the index of the argument in the list. -1 if it's not in the list
int argIndex(String arg){
	if(args!=null){
	for(int i=0; i<args.length; i++){
		if(arg.equals(args[i])){
			return i;
		}
	}
	}
	return -1;
}

boolean argExists(String arg){
	return argIndex(arg)>=0;
}

String argNext(int i){
	return args[i+1];
}
