#!/bin/bash
cd /home/sejo/processing-3.2.3
path="/home/sejo/Dropbox/Escenaconsejo/Processing/sketchbook/git/GenerativeLogo"
echo $path

for i in {0..999};
do
	printf -v number "%03d" $i
	printf -v output "results/1000w/%s.png" $number
	echo "Generating $output..."
#	./processing-java --sketch=$path --run --png $output --random-background 
	./processing-java --sketch=$path --run --png $output 
done

cd $path
